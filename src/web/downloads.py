#!/usr/bin/python

import logging
import tornado.web

from . import base

class IndexHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		release = self.backend.releases.get_latest()
		if not release:
			raise tornado.web.HTTPError(404)

		# Redirect to latest release page
		self.redirect("/downloads/%s" % release.slug)


class MirrorsHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		mirrors = self.backend.mirrors.get_by_countries()
		if not mirrors:
			raise tornado.web.HTTPError(404)

		self.render("downloads/mirrors.html", mirrors=mirrors)


class ReleaseHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self, slug):
		release = self.backend.releases.get_by_sname(slug)
		if not release:
			raise tornado.web.HTTPError(404)

		self.render("downloads/release.html", release=release)


class ThankYouHandler(base.AnalyticsMixin, base.BaseHandler):
	def get(self):
		url = self.get_argument("url")

		# Send 400 if URL is wrong
		if not url.startswith("https://downloads.ipfire.org/"):
			raise tornado.web.HTTPError(400)

		filename = url.removeprefix("https://downloads.ipfire.org/")

		# Get file by URL
		file = self.backend.releases.get_file_by_filename(filename)
		if not file:
			raise tornado.web.HTTPError(404)

		self.render("downloads/thank-you.html", file=file)


class FileHandler(base.AnalyticsMixin, base.BaseHandler):
	def prepare(self):
		self.set_header("Pragma", "no-cache")

	@base.ratelimit(minutes=5, requests=10)
	def get(self, filename):
		mirror = self.backend.mirrors.get_for_download(filename,
			country_code=self.current_country_code)

		# Send 404 if no mirror was found
		if not mirror:
			raise tornado.web.HTTPError(404, "Could not find a mirror for %s" % filename)

		# Construct the redirection URL
		url = mirror.build_url(filename)
		if not url:
			raise tornado.web.HTTPError(500, "Could not get download URL")

		# Log something
		logging.info("Sending client from %s to %s" % (self.current_country_code or "unknown", url))

		# Redirect the request
		self.redirect(url)

	# Allow HEAD
	head = get

	def write_error(self, status_code, **kwargs):
		"""
			Since we cannot deliver any CSS or similar here,
			we will have to redirect the user back to the main page
		"""
		self.redirect("https://www.ipfire.org/error/%s" % status_code)
