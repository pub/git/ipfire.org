#!/usr/bin/python

import logging
import os
import re
import tornado.httpserver
import tornado.ioloop
import tornado.locale
import tornado.options
import tornado.web
import unicodedata

from . import base
from . import ui_modules

BASEDIR = os.path.dirname(__file__)

LATEST_VERSION = "2.0"

def word_wrap(s, width=45):
	paragraphs = s.split('\n')
	lines = []
	for paragraph in paragraphs:
		while len(paragraph) > width:
			pos = paragraph.rfind(' ', 0, width)
			if not pos:
				pos = width
			lines.append(paragraph[:pos])
			paragraph = paragraph[pos:]
		lines.append(paragraph.lstrip())
	return '\n'.join(lines)

class BootBaseHandler(base.BaseHandler):
	@property
	def arch(self):
		arch = self.get_argument("arch", "i386")
		if not arch in ("x86_64", "i586", "i386"):
			raise tornado.web.HTTPError(400, "Invalid architecture")

		if arch == "i386":
			arch = "i586"

		return arch

	@property
	def platform(self):
		platform = self.get_argument("platform", "pcbios")
		if not platform in ("pcbios", "efi"):
			raise tornado.web.HTTPError(400, "Invalid platform")

		return platform


class MenuGPXEHandler(BootBaseHandler):
	"""
		menu.gpxe
	"""
	def get(self):
		version = self.get_argument("version", None)
		if not version or version < LATEST_VERSION:
			return self.serve_update()

		# Deliver content
		self.set_header("Content-Type", "text/plain")
		self.write("#!gpxe\n")

		self.write("set 209:string premenu.cfg?arch=%s&platform=%s\n" \
			% (self.arch, self.platform))
		self.write("set 210:string http://boot.ipfire.org/\n")
		self.write("chain pxelinux.0\n")

	def serve_update(self):
		self.set_header("Content-Type", "text/plain")
		self.write("#!gpxe\n")

		# Small warning
		self.write("echo\necho Your copy of gPXE/iPXE is too old. ")
		self.write("Upgrade to avoid seeing this every boot!\n")

		self.write("chain static/netboot/ipxe.kpxe\n")


class PremenuCfgHandler(BootBaseHandler):
	def get(self):
		self.set_header("Content-Type", "text/plain")

		self.render("netboot/premenu.cfg", arch=self.arch, platform=self.platform)


class MenuCfgHandler(BootBaseHandler):
	class NightlyBuildReleaseDummy(object):
		id = 100
		name = "Nightly Build"
		sname = "nightly-build"

		def supports_arch(self, arch):
			return arch in ("i586", "x86_64")

		def supports_platform(self, platform):
			return platform == "pcbios"

		def netboot_kernel_url(self, arch, platform):
			return "http://boot.ipfire.org/nightly/next/latest/%s/images/vmlinuz" % arch

		def netboot_initrd_url(self, arch, platform):
			return "http://boot.ipfire.org/nightly/next/latest/%s/images/instroot" % arch

		def netboot_args(self, arch, platform):
			return "installer.download-url=https://nightly.ipfire.org/next/latest/%s/images/installer.iso" % arch

		def is_netboot_capable(self):
			return True

	nightly_build = NightlyBuildReleaseDummy()

	def get(self):
		self.set_header("Content-Type", "text/plain")

		latest_release = self.releases.get_latest()
		if latest_release and not latest_release.supports_arch(self.arch):
			latest_release = None

		stable_releases = [r for r in self.releases.get_stable()
			if r.supports_arch(self.arch) and r.supports_platform(self.platform)]
		try:
			stable_releases.remove(latest_release)
		except ValueError:
			pass

		development_releases = [r for r in self.releases.get_unstable()
			if r.supports_arch(self.arch) and r.supports_platform(self.platform)]
		development_releases.insert(0, self.nightly_build)

		self.render("netboot/menu.cfg", latest_release=latest_release,
			stable_releases=stable_releases, development_releases=development_releases,
			arch=self.arch, platform=self.platform)


class MenuConfigModule(ui_modules.UIModule):
	def render(self, release, arch=None, platform=None):
		return self.render_string("netboot/menu-config.cfg", release=release,
			arch=arch, platform=platform)


class MenuHeaderModule(ui_modules.UIModule):
	def render(self, title, releases, arch=None, platform=None):
		# Remove any non-ASCII characters
		try:
			slug = unicodedata.normalize("NFKD", title)
		except TypeError:
			pass

		# Remove excessive whitespace
		slug = re.sub(r"[^\w]+", " ", slug)

		slug = "-".join(slug.split()).lower()

		return self.render_string("netboot/menu-header.cfg", slug=slug,
			title=title, releases=releases, arch=arch, platform=platform)


class MenuSeparatorModule(ui_modules.UIModule):
	def render(self):
		return self.render_string("netboot/menu-separator.cfg")
