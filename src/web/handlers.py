#!/usr/bin/python3

from . import base

class IndexHandler(base.AnalyticsMixin, base.BaseHandler):
	"""
		This handler displays the welcome page.
	"""
	def get(self):
		# Get the latest release.
		latest_release = self.releases.get_latest()

		return self.render("index.html", latest_release=latest_release)


class StaticHandler(base.AnalyticsMixin, base.BaseHandler):
	def initialize(self, template):
		self._template = template

	def get(self):
		self.render("%s" % self._template)
