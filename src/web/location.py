#!/usr/bin/python3

from .. import util
from . import base

class LookupHandler(base.AnalyticsMixin, base.BaseHandler):
	async def get(self, address):
		# Lookup address
		address = util.Address(self.backend, address)

		self.render("location/lookup.html", address=address)
