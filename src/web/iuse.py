#!/usr/bin/python

import logging
import tornado.web

from . import base

class ImageHandler(base.AnalyticsMixin, base.BaseHandler):
	def write_error(self, status_code, **kwargs):
		"""
			Select a random image from the errors directory
			and return the content.
		"""
		self.set_expires(3600)

		# Redirect to static image
		self.redirect(self.static_url("img/iuse-not-found.png"))

	def get(self, profile_id, image_id):
		when = self.get_argument_date("when", None)

		profile = self.fireinfo.get_profile(profile_id, when=when)
		if not profile:
			raise tornado.web.HTTPError(404, "Profile '%s' was not found." % profile_id)

		logging.info("Rendering new image for profile: %s" % profile_id)

		image_cls = self.iuse.get_imagetype(image_id)
		if not image_cls:
			raise tornado.web.HTTPError(404, "Image class is unknown: %s" % image_id)

		# Render the image
		image = image_cls(self.backend, self, profile).to_string()

		# Cache generate images for 3 hours
		self.set_expires(3600 * 3)

		self.set_header("Content-Type", "image/png")
		self.write(image)
