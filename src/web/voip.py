#!/usr/bin/python3

import asyncio
import tornado.web

from . import base
from . import ui_modules

class IndexHandler(base.BaseHandler):
	@tornado.web.authenticated
	async def get(self):
		# Only staff can view this page
		if not self.current_user.is_staff():
			raise tornado.web.HTTPError(403)

		# Fetch everything
		registrations, outbound_registrations, queues, conferences, = \
			await asyncio.gather(
				self.backend.asterisk.get_registrations(),
				self.backend.asterisk.get_outbound_registrations(),
				self.backend.asterisk.get_queues(),
				self.backend.asterisk.get_conferences(),
			)

		self.render("voip/index.html", registrations=registrations, queues=queues,
			outbound_registrations=outbound_registrations, conferences=conferences)


class OutboundRegistrationsModule(ui_modules.UIModule):
	def render(self, registrations):
		return self.render_string("voip/modules/outbound-registrations.html",
			registrations=registrations)


class RegistrationsModule(ui_modules.UIModule):
	def render(self, registrations):
		return self.render_string("voip/modules/registrations.html",
			registrations=registrations)


class QueuesModule(ui_modules.UIModule):
	def render(self, queues):
		return self.render_string("voip/modules/queues.html", queues=queues)


class ConferencesModule(ui_modules.UIModule):
	def render(self, conferences):
		return self.render_string("voip/modules/conferences.html", conferences=conferences)
