#!/usr/bin/python

import ipaddress
import pycares
import tornado.gen
import tornado.platform.caresresolver

class Resolver(tornado.platform.caresresolver.CaresResolver):
	def initialize(self, **kwargs):
		super().initialize()

		# Overwrite Channel
		self.channel = pycares.Channel(sock_state_cb=self._sock_state_cb, **kwargs)

	async def query(self, name, type=pycares.QUERY_TYPE_A):
		# Create a new Future
		fut = tornado.gen.Future()

		# Perform the query
		self.channel.query(name, type, lambda result, error: fut.set_result((result, error)))

		# Wait for the response
		result, error = await fut

		# Handle any errors
		if error:
			# NXDOMAIN
			if error == pycares.errno.ARES_ENOTFOUND:
				return

			# Ignore responses with no data
			elif error == pycares.errno.ARES_ENODATA:
				return

			raise IOError(
				"C-Ares returned error %s: %s while resolving %s"
				% (error, pycares.errno.strerror(error), name)
			)

		# Return the result
		return result
