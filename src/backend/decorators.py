#!/usr/bin/python3

class Missing(object):
	pass

_missing = Missing()

class lazy_property(property):
	"""
		The property is only computed once and then being
		cached until the end of the lifetime of the object.
	"""
	def __init__(self, fget, fset=None, fdel=None, doc=None, name=None):
		property.__init__(self, fget=fget, fset=fset, fdel=fdel, doc=doc)

		self.__name__   = name or fget.__name__
		self.__module__ = fget.__module__

	def __get__(self, obj, type=None):
		if object is None:
			return self

		value = obj.__dict__.get(self.__name__, _missing)
		if value is _missing:
			obj.__dict__[self.__name__] = value = self.fget(obj)

		return value

	def __set__(self, obj, value):
		if self.fset:
			self.fset(obj, value)

		obj.__dict__[self.__name__] = value
