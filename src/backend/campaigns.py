#!/usr/bin/python3

import datetime

from .misc import Object

class Campaigns(Object):
	async def send(self, template, promotional=True, **kwargs):
		"""
			Sends a message to all users that have consented to promotional messages
		"""
		for account in self.backend.accounts.subscribed:
			# Send the message
			account.send_message(template, **kwargs)

			# Remember that we sent a promotional message
			if promotional:
				account.promotional_message_sent()

	async def donate(self):
		"""
			Runs the donation campain, i.e. sends an email once every two months.
		"""
		t = datetime.datetime.now() - datetime.timedelta(days=60)

		for account in self.backend.accounts.subscribed:
			# Fall through if we have no timestamp
			if account.last_promotional_message_sent_at is None:
				pass

			# Fall through if we have passed the threshold
			elif account.last_promotional_message_sent_at <= t:
				pass

			# If nothing matched, we skip this user
			else:
				continue

			# Send a donation reminder
			account.send_message("auth/messages/donation-reminder")

			# Remember that we sent a promotional message
			account.promotional_message_sent()
