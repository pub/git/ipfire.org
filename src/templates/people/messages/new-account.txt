From: IPFire Project <no-reply@ipfire.org>
To: IPFire Moderators <moderators@ipfire.org>
Subject: {{ _("New Account Registered: %s") % account }}

{{ _("Hello,") }}

{{ _("a new account has been registered:") }}

* {{ _("UID: %s") % account.uid }}
* {{ _("Name: %s") % account }}
* {{ _("Email: %s") % account.email }}

{{ _("More can be found here:") }}

  https://www.ipfire.org/users/{{ account.uid }}
