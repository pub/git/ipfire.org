From: IPFire Project <no-reply@ipfire.org>
To: {{ account.email_to }}
Subject: 🎄 Make a Lasting Impact This Holiday Season—Support IPFire
Precedence: bulk
X-Auto-Response-Suppress: OOF

{{ _("Dear %s,") % account.first_name }}

As the holiday season draws to a close, we need your help to make 2025 even stronger for
IPFire and the businesses who rely on us. This is the final opportunity to contribute to
our year-end goal—and your donation will make all the difference.

With your support, we can:

  * Develop new features and improvements that keep businesses secure
  * Protect against the latest cyber threats—ensuring uptime and data integrity for organizations worldwide
  * Expand the IPFire community, providing more support to those who rely on Open Source solutions for their network security

  https://www.ipfire.org/donate?utm_medium=email&utm_campaign=christmas-3

Now is the time to act. Without your support, we won't be able to meet our 2024 goals,
and we could lose valuable momentum heading into the new year.

Thank you for your generosity—and for standing with us during this crucial time. Together,
we can make 2025 a year of stronger, safer networks for all.

{{ _("Thank you,") }}
-{{ _("Your IPFire Team") }}

--
{{ _("Don't like these emails?") }} https://www.ipfire.org/unsubscribe
